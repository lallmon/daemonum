extends Control

func _ready():
	# warning-ignore:return_value_discarded
	Events.connect("start_game", self, "_on_Game_start")

func _process(_delta):
	if Input.is_action_just_released("ui_accept"):
		Events.emit_signal("start_game")
	
func _on_Game_start():
	queue_free()
