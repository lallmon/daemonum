extends Node

# warning-ignore:unused_signal
signal blood_total_change
# warning-ignore:unused_signal
signal blood_level_zero
# warning-ignore:unused_signal
signal enemy_is_dead(point_value, position)
# warning-ignore:unused_signal
signal game_over(final_score)
# warning-ignore:unused_signal
signal player_cast_spells
# warning-ignore:unused_signal
signal player_is_dead
# warning-ignore:unused_signal
signal start_game
