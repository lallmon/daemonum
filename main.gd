extends Node

onready var first_level = preload("res://levels/level_00/level_00.tscn")
onready var game_gui = preload("res://gui/gui.tscn")
onready var game_over = preload("res://gui/screens/game_over.tscn")
onready var start_screen = preload("res://gui/screens/game_start.tscn")

func _ready():
	#only need to call randomize once per start of project
	randomize()
	# warning-ignore:return_value_discarded
	Events.connect("start_game", self, "_on_Game_start")
	# warning-ignore:return_value_discarded
	Events.connect("game_over", self, "_on_Game_is_over")
	$UI.add_child(start_screen.instance())

func _process(_delta):
	if Input.is_action_just_released("full_screen"):
		OS.window_fullscreen = true

func _on_Game_start():
	$World/Level.add_child(first_level.instance())

func _on_Game_is_over(final_score : int) -> void:
	var over = game_over.instance()
	over.final_score = final_score
	$UI.add_child(over)
	$World/Level.get_child(0).queue_free()

func _on_Despawner_area_exited(area):
	if area.is_in_group("enemy") or area.is_in_group("enemy_spells"):
		area.queue_free()
