extends Area2D

export var base_speed : float = 800
export var base_damage : int = 50 

func _ready():
	$AnimationPlayer.play("is_cast")
	add_to_group("player_spells")

func _physics_process(delta):
	position += transform.x * base_speed * delta
	
func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
