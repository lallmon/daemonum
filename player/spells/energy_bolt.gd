extends Position2D

var pl_fireball = preload("res://player/spells/projectiles/fireball.tscn")

func _ready():
# warning-ignore:return_value_discarded
	Events.connect("player_cast_spells", self, "_on_Player_cast_spells")

func _on_Player_cast_spells():
	$AudioStreamPlayer.play(0.0)
	for point in $CastPoints.get_children():
		var fireball = pl_fireball.instance()
		fireball.transform = point.global_transform
		get_tree().current_scene.add_child(fireball)	

