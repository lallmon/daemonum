extends Node2D

var _choices = [0.75, 1, 1.25, 1.5]

func _ready():
	var sprite = _choices[randi() % _choices.size()]
	$AudioStreamPlayer2D.pitch_scale = _choices[randi() % _choices.size()]
	$AnimatedSprite.scale = Vector2(sprite, sprite) #even scaling
	$AnimatedSprite.play("00")
	$AudioStreamPlayer2D.play(0.0)
	
func _on_AnimatedSprite_animation_finished():
	queue_free()
