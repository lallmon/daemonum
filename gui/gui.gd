extends Control

var _blood_progress : float = 0
var _next_level : int = 2
var _score : int = 0

onready var meter_field := $PlayerInfo/Layout/Multiplier/Layout/Value
onready var meter_progress := $PlayerInfo/Layout/Multiplier/ProgressBar
onready var popup_score := preload("res://gui/components/popup_score.tscn")
onready var score_field := $PlayerInfo/Layout/Score/Value

func _ready():
	# warning-ignore:return_value_discarded
	Events.connect("blood_total_change", self, "_on_Blood_total_change")
	# warning-ignore:return_value_discarded
	Events.connect("enemy_is_dead", self, "_on_Enemy_is_dead")
	# warning-ignore:return_value_discarded
	Events.connect("player_is_dead", self, "_on_Player_is_dead")

func _blood_check():
	Blood.set_level(Blood.get_total())
	#get total as a float and subtract level, to generate a fraction between 
	#0 and 1, to set the progress bar!
	meter_progress.value = float(Blood.get_total()) / 100 - Blood.get_level()
	meter_field.text = String(Blood.get_level())

	if Blood.get_level() <= 0:
		meter_progress.value = 0
	elif Blood.get_level() < _next_level:
		_next_level -= 1
		#TODO: Put animations back in reliably
	elif Blood.get_level() == _next_level:
		_next_level += 1
		#TODO: here as well

func _reset_scoring():
	Blood.set_level(1)
	_blood_progress = 0
	Blood.set_total(100)
	_next_level = 2
	_score = 0
	score_field.text = String(_score)
	meter_field.text = String(Blood.get_level())
	_blood_check()

func _on_Enemy_is_dead(point_value, position):
	var _new_score = point_value * Blood.get_level()
	_score += _new_score
	var pop = popup_score.instance()
	pop.score = _new_score
	pop.rect_position = position
	add_child(pop)
	score_field.text = String(_score)
	

func _on_Player_is_dead():
	#Underscore prefixes mean private variables, but also mean for functions
	#To not use them, so I have to reassign a variable here lol.
	var final_score = _score
	Events.emit_signal("game_over", final_score)
	_reset_scoring()

func _on_Blood_total_change():
	_blood_check()
