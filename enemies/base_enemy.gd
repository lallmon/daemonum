#IMPORTANT: When you make an inherited scene from this, you need to detach this
#and add a new script that extends base_enemy.gd 
extends Node2D
class_name BaseEnemy

var death_effect = preload("res://enemies/effects/is_dead.tscn")

export var base_health : int = 250
export var base_point_value : int = 500
export var movement_speed : int = 60

onready var cast_point = $Area2D/CastPoint

func _ready():
	$AnimationTree.active = true

func _check_health() -> void:
	if base_health <= 0:
		#call _handle_death() in the animation
		$AnimationTree.active = false
		$AnimationPlayer.play("is_dead") 

func _handle_death() -> void:
	var sprite_position = $Area2D/AnimatedSprite.global_position
	var death = death_effect.instance()
	death.position = sprite_position
	get_parent().add_child(death)
	Events.emit_signal("enemy_is_dead", base_point_value, sprite_position)

func _on_VisibilityNotifier2D_screen_exited():
	if global_position.x < 0:
		queue_free()

func _on_Area2D_area_entered(projectile):
	if projectile.is_in_group("player_spells"):
		#TODO: create "cancel" method on spell projectiles
		projectile.queue_free()
		$AnimationPlayer.play("is_hit")
		base_health -= projectile.base_damage
		_check_health()
