extends BaseEnemy

var patterns = [preload("res://enemies/spells/patterns/two_spread.tscn"),
				preload("res://enemies/spells/patterns/three_spread.tscn")]
var pl_lightning = preload("res://enemies/spells/lightning_ball.tscn")

func _ready():
	cast_point.add_child(patterns[0].instance())
	movement_speed = movement_speed + randi() % 50 + 30

func _physics_process(delta):
	position -= transform.x * movement_speed * delta

func change_level(level : int) -> void:
	match level:
		1:
			base_health = 400
			base_point_value = 1500
			cast_point.get_child(0).queue_free()
			cast_point.add_child(patterns[level].instance())
			$CastDelay.wait_time = 1
			modulate = Color.from_hsv(.9, .68, 1, 1)

func _cast_spells() -> void:
	if cast_point:
		for source in cast_point.get_child(0).get_children():
			var ball = pl_lightning.instance()
			ball.add_to_group('enemy_spells')
			get_parent().add_child(ball)
			ball.transform = source.global_transform

func _on_CastDelay_timeout():
	$AnimationPlayer.play("is_casting")

func _on_VisibilityNotifier2D_screen_entered():
	$CastDelay.start()
