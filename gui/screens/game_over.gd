extends Control

var final_score

func _ready():
	$RestartButton.disabled = true
	$FinalScore/Value.text = String(final_score)
	$AnimationPlayer.play("is_over")

func _process(_delta):
	if Input.is_action_just_released("ui_accept") and not $RestartButton.disabled:
		Events.emit_signal("start_game")
		queue_free()

func _on_RestartButton_pressed():
	Events.emit_signal("start_game")
	queue_free()
