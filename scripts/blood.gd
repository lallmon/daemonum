extends Node

var _blood_level : int = 1 setget set_level, get_level
var _blood_total : int = 100

func get_level():
	return _blood_level

func set_level(value):
	_blood_level = int(value / 100)
	if _blood_level <= 0:
		_blood_level = 0
		Events.emit_signal("blood_level_zero")

func get_total():
	return _blood_total

func set_total(value):
	_blood_total = value
	Events.emit_signal("blood_total_change")

