extends KinematicBody2D

var animation_state
var velocity = Vector2.ZERO

export var acceleration : int = 35
export var cast_rate : float = 0.25
export var friction : int = 120
export var max_speed : int = 310


onready var cast_delay := $CastDelay

func _ready():
	animation_state = $AnimationTree.get("parameters/playback")
	$AnimationTree.active = true
		# warning-ignore:return_value_discarded
	Events.connect("blood_level_zero", self, "_on_Blood_level_zero")

func _physics_process(_delta):
	var input = Vector2.ZERO
	input.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	input.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	if input != Vector2.ZERO:
		velocity += input.normalized() * acceleration
		velocity = velocity.clamped(max_speed)
	else:
		velocity = velocity.move_toward(Vector2.ZERO, friction)
	# warning-ignore:return_value_discarded
	move_and_slide(velocity)

func _process(_delta):
	if Input.is_action_pressed("shoot") and cast_delay.is_stopped():
		cast_delay.start(cast_rate)
		Events.emit_signal("player_cast_spells")

	if Input.is_action_pressed("move_left"):
		animation_state.travel("is_moving_left")
	elif Input.is_action_pressed("move_right"):
		animation_state.travel("is_moving_right")
	else:
		animation_state.travel("is_idle")

func _handle_death():
	Events.emit_signal("player_is_dead")

func _on_PickupRadius_area_entered(area):
	#TODO: When it's here, move collectable towards the player and collect it when it hits the
	if area.is_in_group("blood_drop"):
		area.was_collected()

func _on_Blood_level_zero():
	$AnimationTree.active = false
	$AnimationPlayer.play("is_dead")
	
func _on_Character_area_entered(area):
	if area.is_in_group('enemy_spells'):
		$AnimationPlayer.play("is_hit")
		#TODO move damage into the bullet properties
		Blood.set_total(Blood.get_total() - 10)
		area.queue_free()
	elif area.is_in_group('enemy'):
		$AnimationTree.active = false
		$AnimationPlayer.play("is_dead")
