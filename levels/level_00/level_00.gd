extends Node

export var background_scroll_speed : int = 150

var pl_enemy = preload("res://enemies/types/skull/flying_skull.tscn")
var pl_drop = preload("res://enemies/pickups/blood/drop.tscn")
var can_spawn_enemies = true

onready var battle = $BattleLayer

func _ready():
	$AnimationPlayer.play("mist_float")
	$AudioStreamPlayer.play(0.0)
# warning-ignore:return_value_discarded
	Events.connect("player_is_dead", self, "_on_Player_is_dead")
# warning-ignore:return_value_discarded
	Events.connect("enemy_is_dead", self, "_on_Enemy_is_dead")

func _process(delta):
	$ParallaxBackground.scroll_base_offset.x -= background_scroll_speed * delta

func _clear_enemies():
	can_spawn_enemies = false
	get_tree().call_group("enemy", "queue_free")
	get_tree().call_group("enemy_spells", "queue_free")

func _spawn_enemies(amount : int, level : int):
	var size = get_viewport().get_visible_rect().size
	for _i in range(0, amount):
		var enemy = pl_enemy.instance()
		enemy.position.x = size.x + background_scroll_speed * 1.25
		enemy.position.y = int(rand_range(48, size.y - 48))
		battle.add_child(enemy)
		enemy.change_level(level)

func _on_Enemy_is_dead(_point_value, position : Vector2) -> void:
	var drop = pl_drop.instance()
	drop.position = position
	battle.add_child(drop)

func _on_EnemySpawn_timeout():
	if can_spawn_enemies:
		_spawn_enemies(int(rand_range(1, 3)), int(rand_range(0, 6)))
		$EnemySpawn.wait_time = int(rand_range(2, 4))

func _on_Player_is_dead():
	$AudioStreamPlayer.stop()
	_clear_enemies()
