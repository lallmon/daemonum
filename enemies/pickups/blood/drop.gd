extends Area2D

export var movement_speed : int = 60
export var value : int = 25

func _ready():
	add_to_group("blood_drop")
	$AnimationPlayer.play("is_degrading")

func _physics_process(delta):
	position -= transform.x * movement_speed * delta

func was_collected():
	$AnimationPlayer.play("was_collected")
	Blood.set_total(Blood.get_total() + value)
