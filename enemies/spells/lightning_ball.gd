extends Area2D

export var speed: float = 270

func _physics_process(delta):
	#needs to be negative since its used by enemies
	position -= transform.x * speed * delta

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

